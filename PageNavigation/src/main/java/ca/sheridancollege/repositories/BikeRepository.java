package ca.sheridancollege.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ca.sheridancollege.beans.Bike;

public interface BikeRepository extends CrudRepository<Bike, Integer> {
	
List<Bike> findById(int id);
}
