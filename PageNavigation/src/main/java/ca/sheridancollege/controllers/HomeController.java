package ca.sheridancollege.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import ca.sheridancollege.beans.Bike;
import ca.sheridancollege.repositories.BikeRepository;

@Controller
public class HomeController {
	@Autowired
	private BikeRepository bikeRepos;
	@GetMapping("/")
	public String Home() {
		
		return "Home.html";
	}
	@GetMapping("/showBikes")
	public String showBikes(Model model) {
		model.addAttribute("bindBike",	bikeRepos.findAll());
		return "ShowBikes.html";
	}
	
	@GetMapping("/AddBike")
	public String addBike(@ModelAttribute Bike bike, Model model) {
		
		bikeRepos.save(bike);
		model.addAttribute("bindBike", bikeRepos.findAll());
		model.addAttribute("bike", new Bike());
		return "ShowBikes.html";
		
	}
	@GetMapping("/GoViewBikes")
	public String viewBike(Model model) {
		model.addAttribute("bindBike", bikeRepos.findAll());
		return "ShowBikes.html";
	}
	@GetMapping("/edit/{id}")
	public String editBike(@PathVariable int id, Model model) {
		if(bikeRepos.findById(id).size()>0) {
			Bike bike = bikeRepos.findById(id).get(0);
			model.addAttribute("bindBike", bike);
			return "EditBike.html";
		}
		return "redirect:/";
	}
	@GetMapping("/modifyBike")
	public String modifyBike(@ModelAttribute Bike bike, Model model) {
		bikeRepos.save(bike);		
		model.addAttribute("bindBike",	bikeRepos.findAll());
		return "ShowBikes.html";
	}
}
