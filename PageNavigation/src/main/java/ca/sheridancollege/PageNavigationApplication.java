package ca.sheridancollege;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PageNavigationApplication {

	public static void main(String[] args) {
		SpringApplication.run(PageNavigationApplication.class, args);
	}

}
